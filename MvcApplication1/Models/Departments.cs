﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    [MetadataType(typeof(DepartmentModel))]
    public partial class Department
    {
      
    }
    public class DepartmentModel
    {
        [Display(Name = "Department Name")]
        public string Name { get; set; }
    }
}